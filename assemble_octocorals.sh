#!/bin/bash

~/trinityrnaseq-v2.14.0/Trinity --seqType fq --max_memory 40G --left SRR14348000_1.fastq.gz --right SRR14348000_2.fastq.gz --CPU 16 --full_cleanup --trimmomatic --output malacobelemnon_daytoni_trinity > malacobelemnon_daytoni_SRR14348000_trinity.log
# Wrote 74274 sequences: 100.0% of total
# Total input letters is: 58335602
# Total output count is: 58335602
# Ratio of out/in is: 1.0
# Average length is: 785.41
# Median length is: 440
# Shortest sequence is: 182
# Longest sequence is: 10819
# n50 length is: 29168718 and n50 is: 1317.00
# n10 length is: 52503126 and n10 is: 3458.00

~/trinityrnaseq-v2.14.0/Trinity --seqType fq --max_memory 40G --left SRR15783032_1.fastq.gz --right SRR15783032_2.fastq.gz --CPU 16 --full_cleanup --trimmomatic --SS_lib_type RF --output erythropodium_caribaeorum_trinity > erythropodium_caribaeorum_SRR15783032_trinity.log

