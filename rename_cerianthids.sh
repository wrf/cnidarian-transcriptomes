#!/bin/bash


fastarenamer.py -j "|" -p Pachycerianthus_maua -T trinity_Pas.Trinity.fasta | prottrans.py -a 50 -n - | commonseq.py -1 - -e > pachycerianthus_maua_Trinity.prot.fasta

gzip trinity_Pas.Trinity.fasta

fastarenamer.py -j "|" -p Isarachnanthus_nocturnus -T trinity_Isn.Trinity.fasta | prottrans.py -a 50 -n - | commonseq.py -1 - -e > isarachnanthus_nocturnus_Trinity.prot.fasta

gzip trinity_Isn.Trinity.fasta

fastarenamer.py -j "|" -p Ceriantheomorphe_brasiliensis -T trinity_Cebr.Trinity.fasta | prottrans.py -a 50 -n - | commonseq.py -1 - -e > ceriantheomorphe_brasiliensis_Trinity.prot.fasta

gzip trinity_Cebr.Trinity.fasta

fastarenamer.py -j "|" -p Pachycerianthus_borealis -T trinity_Ceam.Trinity.fasta | prottrans.py -a 50 -n - | commonseq.py -1 - -e > pachycerianthus_borealis_Trinity.prot.fasta

gzip trinity_Ceam.Trinity.fasta


 
 
