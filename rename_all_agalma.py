#!/usr/bin/env python

'''
rename all fasta headers from Zapata 2015 transcriptome set
and automatically rename output files
'''

import sys

if len(sys.argv) < 2:
	print >> sys.stderr, "USAGE:\nrename_all_agalma.py *.fasta"
	sys.exit()
else:
	for agalmafasta in sys.argv[1:]:
		prefix = agalmafasta.rsplit("_",1)[0]
		outfile = agalmafasta.replace(".fasta",".renamed.fasta")
		with open(outfile,'w') as fa:
			for line in open(agalmafasta).readlines():
				if line[0]==">":
			
					newline = ">{}_{}".format(prefix, line[1:].split(" ")[0].replace("agalma/gene:","g").replace("/isoform:","_i") )
					print >> fa, newline
				else:
					fa.write(line)