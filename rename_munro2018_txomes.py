#!/usr/bin/env python

# rename_munro2018_txomes.py 2020-03-16

'''
rename_munro2018_txomes.py *.fa

rename all transcriptome files from the Munro 2018 dataset
and rename the sequences with fasta header for each species

tar.gz transcripts are downloaded here:
https://github.com/caseywdunn/siphonophore_phylogeny_2017/tree/master/transcriptomes

species names come from index.csv file downloaded here:
https://github.com/caseywdunn/siphonophore_phylogeny_2017/blob/master/transcriptomes/index.csv
'''

import sys

if len(sys.argv) < 2:
	sys.exit( __doc__ )
else:
	rename_dict = {"HWI-ST625-54-C026EACXX-7-TGACCA": "Marrus claudanielis",
    "HWI-ST625-54-C026EACXX-6-TTAGGC": "Apolemia rubriversa",
    "HWI-ST625-54-C026EACXX-6-GCCAAT": "Chuniphyes multidentata",
    "HWI-ST625-159-C4MVCACXX-5-AGTTCC": "Apolemia sp",
    "HWI-ST625-159-C4MVCACXX-5-CCGTCC": "Apolemia lanosa",
    "HWI-ST625-51-C02UNACXX-8-BARGMANNIA": "Bargmannia elongata",
    "K00162-189-HJTYGBBXX-7-NCAGTG": "Diphyes dispar",
    "SRX231866": "Aiptasia pallida",
    "HWI-ST625-54-C026EACXX-8-ACTTGA": "Stephalia dilata",
    "HWI-ST625-73-C0JUVACXX-7-TTAGGC": "Physalia physalis",
    "HWI-ST625-159-C4MVCACXX-5-GTCCGC": "Bargmannia amoena",
    "HWI-ST625-51-C02UNACXX-6-FRILLAGALMA": "Frillagalma vityazi",
    "HISEQ-168-C3DEYACXX-8-ALATINA": "Alatina alata",
    "dbEST_CLYHEM": "Clytia hemisphaerica",
    "HWI-ST420-69-D0F2NACXX-3-ECTOPLEURA": "Ectopleura larynx",
    "HWI-ST625-181-C76K5ACXX-2-CTTGTA": "Athorybia rosacea",
    "HWI-ST625-54-C026EACXX-7-GGCTAC": "Forskalia asymmetrica",
    "HWI-ST625-54-C026EACXX-7-CTTGTA": "Prayidae D27D2",
    "HWI-ST625-54-C026EACXX-7-CGATGT": "Lilyopsis fluoracantha",
    "HISEQ-129-C25BFACXX-8-HYDRACTINIA": "Hydractinia symbiolongicarpus",
    "HWI-ST625-54-C026EACXX-8-CAGATC": "Rhizophysa filiformis",
    "HWI-ST625-73-C0JUVACXX-7-AGALMA2": "Agalma elegans",
    "HWI-ST625-54-C026EACXX-6-ACAGTG": "Chelophyes appendiculata",
    "HWI-ST625-54-C026EACXX-7-GATCAG": "Prayidae D27SS7",
    "HWI-ST625-181-C76K5ACXX-2-ATCACG": "Rudjakovia sp",
    "HWI-ST625-54-C026EACXX-6-ATCACG": "Abylopsis tetragona",
    "JGI_NEMVEC": "Nematostella vectensis",
    "HWI-ST625-54-C026EACXX-8-ATCACG": "Hippopodius hippopus",
    "HWI-ST625-181-C76K5ACXX-2-TAGCTT": "Physophora gilmeri",
    "HWI-ST625-159-C4MVCACXX-5-TGACCA": "Bargmannia lata",
    "HWI-ST625-51-C02UNACXX-7-NANOMIA": "Nanomia bijuga",
    "HWI-ST625-54-C026EACXX-7-TAGCTT": "Erenna richardi",
    "HWI-ST625-54-C026EACXX-8-ACAGTG": "Physonect sp ",
    "HWI-ST625-181-C76K5ACXX-2-ACTTGA": "Thermopalia sp",
    "HWI-ST625-54-C026EACXX-6-CAGATC": "Cordagalma sp",
    "HWI-ST625-54-C026EACXX-8-TTAGGC": "Lychnagalma utricularia",
    "HWI-ST625-73-C0JUVACXX-7-ATCACG": "Kephyes ovata",
    "NCBI_HYDMAG": "Hydra magnipapillata",
    "HWI-ST625-54-C026EACXX-8-GCCAAT": "Resomia ornicephala",
    "HWI-ST625-181-C76K5ACXX-2-GGCTAC": "Halistemma rubrum",
    "HISEQ-134-C27MTACXX-2-PODOCORYNA": "Podocoryna carnea" }

	filelist = sys.argv[1:]

	for infilename in filelist:
		sp_index = infilename.rsplit(".",1)[0] # should remove .fa
		sp_name = rename_dict.get(sp_index, None)
		if sp_name is None:
			sys.stderr.write("# WARNING: no index for {}, skipping\n".format( sp_index ) )
			continue
		else:
			sp_name = sp_name.replace(" ","_")
		outfilename = "{}_{}.fasta".format( sp_name, sp_index )
		sys.stderr.write("# Reading {}, writing to {}\n".format( infilename, outfilename ) )
		with open(outfilename,'w') as of:
			for line in open(infilename, 'r'):
				if line[0]==">":
					geneid = line[1:]
					outline = ">{}_{}".format( sp_name, geneid )
					of.write( outline )
				else:
					of.write( line )
	sys.stderr.write("# Done\n")


















